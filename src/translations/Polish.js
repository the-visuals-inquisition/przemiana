/**
 * ============================================================
 * Polish
 * ============================================================
 *
 * Translators:
 *
 * Orzecho <firma@mdabrowski.eu>
 */



export default {
	'AdvanceHelp': 'By iść do przodu kliknij lewym przyciskiem myszy, dotknij ekranu lub wciśnij spację',
	'AllowPlayback': 'Kliknij tutaj by zezwolnić na odtwarzanie audio',
	'Audio': 'Audio',
	'AutoPlay': 'Auto',
	'AutoPlayButton': 'Włącz autoodtwarzanie',
	'AutoPlaySpeed': 'Prędkość autoodtwarzania',

	'Back': 'Powrót',
	'BackButton': 'Powróć',

	'Cancel': 'Anuluj',
	'Close': 'Zamknij',
	'Confirm': 'Czy na pewno chcesz wyjść z gry?',
	'Credits': 'Autorzy',

	'Delete': 'Usuń',
	'DialogLogButton': 'Pokaż dialogi',

	'FullScreen': 'Tryb pełnoekranowy',

	'Gallery': 'Galeria',

	'Help': 'Pomoc',
	'Hide': 'Ukryj',
	'HideButton': 'Ukryj pole tekstowe',

	'iOSAudioWarning': 'Ustawienia audio nie są wspierane na iOS',

	'KeyboardShortcuts': 'Skróty klawiszowe',

	'Language': 'Język',
	'Load': 'Wczytaj',
	'LoadAutoSaveSlots': 'Auto Zapisy',
	'LoadButton': 'Otwórz ekran ładowania',
	'Loading': 'Ładowanie',
	'LoadingMessage': 'Proszę czekaj, trwa ładowanie zasobów',
	'LoadSlots': 'Zapisane gry',
	'LocalStorageWarning': 'Local Storage nie jest wspierane przez twoją przeglądarkę',
	'Log': 'Log',

	'Music': 'Głośność muzyki',

	'NewContent': 'Dostępna jest nowa wersja gry, przeładuj stronę by ją wczytać.',
	'NoSavedGames': 'Brak zapisanych gier',
	'NoAutoSavedGames': 'Brak automatycznie zapisanych gier',
	'NoDialogsAvailable': 'Brak dostępnych dialogów. Dialogi pojawią się tu gdy będą dostępne.',

	'OK': 'OK',
	'OrientationWarning': 'Proszę obróć swoje urządzenie by zagrać.',
	'Overwrite': 'Nadpisz',

	'QuickButtons': 'Przyciski szybkiego menu',
	'QuickMenu': 'Szybkie menu',
	'Quit': 'Wyjdź',
	'QuitButton': 'Wyjdź z gry',

	'Resolution': 'Rozdzielczość',

	'Save': 'Zapisz',
	'SaveButton': 'Otwórz ekran zapisywania',
	'SaveInSlot': 'Zapisz w slocie',
	'SelectYourLanguage': 'Wybierz język',
	'Settings': 'Ustawienia',
	'SettingsButton': 'Otwórz ekran ustawień',
	'Show': 'Pokaż',
	'Skip': 'Pomiń',
	'SkipButton': 'Wejdź w tryb pomijania',
	'SlotDeletion': 'Jesteś pewny, że chcesz usunąć ten slot?',
	'SlotOverwrite': 'Jesteś pewny, że chcesz nadpisać ten slot?',
	'Sound': 'Głośność dźwięku',
	'Start': 'Start',
	'Stop': 'Stop',

	'TextSpeed': 'Prędkość tekstu',

	'Video': 'Głośność wideo',
	'Voice': 'Głośność dialogów',

	'Windowed': 'W oknie'
};
