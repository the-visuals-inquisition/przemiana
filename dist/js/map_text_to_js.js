#!/usr/bin/env node
const fs = require('fs');
fs.readFile('script.txt', 'utf8', function (err,data) {
	if (err) {
		return console.log(err);
	}
	let script = data;
	let index = 1;

	script = script.replace(/^Frankie: (.*)$/mg, function (text) {
		return `say('f', aloesIndex, "${text}"),`;
	});

	script = script.replace(/^Jessie: (.*)$/mg, function (text) {
		return `say('j', ruririIndex, "${text}"),`;
	});

	fs.writeFile('_script.js', script, 'utf8', function (err) {
		if (err) return console.log(err);
	});
});
