/* global monogatari */

// Define the messages used in the game.

function canUseWebP () {
	var elem = document.createElement('canvas');
	if (elem.getContext && elem.getContext('2d')) {
		return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
	}
	return false;
}

var webPSupported = canUseWebP();

monogatari.action('message').messages({
	'Help': {
		title: 'Help',
		subtitle: 'Some useful Links',
		body: `
			<p><a href='https://developers.monogatari.io/documentation/'>Documentation</a> - Everything you need to know.</p>
			<p><a href='https://monogatari.io/demo/'>Demo</a> - A simple Demo.</p>
		`
	}
});

// Define the notifications used in the game
monogatari.action('notification').notifications({
	'Welcome': {
		title: 'Welcome',
		body: 'This is the Monogatari VN Engine',
		icon: ''
	}
});

// Define the Particles JS Configurations used in the game
monogatari.action('particles').particles({});

// Define the canvas objects used in the game
monogatari.action('canvas').objects({});

// Credits of the people involved in the creation of this awesome game
monogatari.configuration('credits', {});


// Define the images that will be available on your game's image gallery
monogatari.assets('gallery', {
	hospital: 'hospital.png',
	training: 'training.png',
	graveyard: 'graveyard.png',
});

// Define the music used in the game.
monogatari.assets('music', {
	'sad': 'music_sad.mp3',
	'beginning': 'music_beginning.mp3',
	'calm': 'music_calm.mp3',
	'nostalgic': 'music_nostalgic.mp3',
	'quarell': 'music_quarell.mp3',
	'ending': 'znow_razem_bedziemy_biec_cichutko.mp3',
});

// Define the voice files used in the game.
monogatari.assets('voices', {});

// Define the sounds used in the game.
monogatari.assets('sounds', {
	traffic: 'traffic.mp3',
	birds: 'birds.mp3',
	whistle: 'whistle.mp3',
	thump: 'thump.mp3',
	heart: 'heart.mp3',
});

// Define the videos used in the game.
monogatari.assets('videos', {});

// Define the images used in the game.
monogatari.assets('images', {});

// Define the backgrounds for each scene.
monogatari.assets('scenes', {
	// bathroom: webPSupported ? 'bathroom.webp' : 'bathroom.png',
	// beachDaySimple: webPSupported ? 'beach_day_simple.webp' : 'beach_day_simple.png',
	// beachDayUmbrella: webPSupported ? 'beach_day_umbrella.webp' : 'beach_day_umbrella.png',
	// beachSunsetSimple: webPSupported ? 'beach_sunset_simple.webp' : 'beach_sunset_simple.png',
	// bedroomNight: webPSupported ? 'bedroom_night.webp' : 'bedroom_night.png',
	// bedroom: webPSupported ? 'bedroom.webp' : 'bedroom.png',
	busStop: webPSupported ? 'bus_stop.webp' : 'bus_stop.png',
	// cafe: webPSupported ? 'cafe.webp' : 'cafe.png',
	// classroomEvening: webPSupported ? 'classroom_evening.webp' : 'classroom_evening.png',
	classroom: webPSupported ? 'classroom.webp' : 'classroom.png',
	// classroomNight: 'classroom_night.png',
	// hallwayHouse: webPSupported ? 'hallway_house.webp' : 'hallway_house.jpg',
	// hallwayHouseNight: webPSupported ? 'hallway_house_night.webp' : 'hallway_house_night.png',
	hallwaySchool: webPSupported ? 'hallway_school.webp' : 'hallway_school.png',
	// hallwaySchoolNight: 'hallway_school_night.png',
	// hallwaySchoolDarker30: 'hallway_school_darker_30.png',
	// hallwaySchoolDarker50: 'hallway_school_darker_50.png',
	// hallwaySchoolDarker60: 'hallway_school_darker_60.png',
	// hallwaySchoolDarker70: 'hallway_school_darker_70.png',
	// hallwaySchoolDarker75: 'hallway_school_darker_75.png',
	// hallwaySchoolDarker80: 'hallway_school_darker_80.png',
	// hallwaySchoolDarker90: 'hallway_school_darker_90.png',
	// hallway: webPSupported ? 'hallway.webp' : 'hallway.jpg',
	// hallway2: webPSupported ? 'hallway2.webp' : 'hallway2.jpg',
	// hallwayNight: webPSupported ? 'hallway_night.webp' : 'hallway_night.jpg',
	// housesEvening: webPSupported ? 'houses_evening.webp' : 'houses_evening.png',
	// housesNight: webPSupported ? 'houses_night.webp' : 'houses_night.png',
	houses: webPSupported ? 'houses.webp' : 'houses.png',
	// japaneseSetting: webPSupported ? 'japanese_setting.webp' : 'japanese_setting.png',
	// kitchen: webPSupported ? 'kitchen.webp' : 'kitchen.png',
	// livingRoom: webPSupported ? 'living_room.webp' : 'living_room.png',
	// livingRoomNight: webPSupported ? 'living_room_night.webp' : 'living_room_night.png',
	// moonLinkFence: webPSupported ? 'moon_link_fence.webp' : 'moon_link_fence.png',
	// moonOverField: webPSupported ? 'moon_over_field.webp' : 'moon_over_field.png',
	// moonPark: webPSupported ? 'moon_park.webp' : 'moon_park.png',
	// moon: webPSupported ? 'moon.webp' : 'moon.png',
	// park: webPSupported ? 'park.webp' : 'park.png',
	// schoolCafeteria: webPSupported ? 'school_cafeteria.webp' : 'school_cafeteria.png',
	// schoolEntranceEvening: webPSupported ? 'school_entrance_evening.webp' : 'school_entrance_evening.png',
	schoolEntrance: webPSupported ? 'school_entrance.webp' : 'school_entrance.png',
	schoolRooftop: webPSupported ? 'school_rooftop.webp' : 'school_rooftop.png',
	// stairs: webPSupported ? 'stairs.webp' : 'stairs.png',
	// kitchenNight: webPSupported ? 'kitchen_night.webp' : 'kitchen_night.png',
	// end: webPSupported ? 'end.webp' : 'end.png',
	// Visuals 02 starts here
	// traditionalPavilion: 'traditional_pavilion.png',
	// traditionalPavilionNight: 'traditional_pavilion_night.png',
	// street1: 'street_1.png',
	// street_1Night: 'street_1Night.png',
	// street2: 'street_2.png',
	// shop1: 'shop_1.png',
	// resto: 'resto.png',
	// nightDistrict1: 'night_district_1.png',
	// nightDistrict2: 'night_district_2.png',
	// mountFuji: 'mount_fuji.png',
	// japaneseDinner: 'japanese_dinner.png',
	// houseNight: 'houseNight.png',
	// houseOld: 'house_old.png',
	// houseOldNight: 'house_old_night.png',
	// cityView: 'city_view.png',
	// time: 'time.jpg',
	// dentalOffice: 'dental_office.png',
	// beach_back: 'beach_back.png',
	// city: 'city.png',
	// sea_night: 'sea_night.png',
	// street_cafe: 'street_cafe.png',
	// beach_road: 'beach_road.png',
	// bunker: 'bunker.jpg',
	hospital: 'hospital.png',
	track: 'track.png',
	track_cinematic: 'track_cinematic.png',
	hospital_cinematic: 'hospital_cinematic.png',
	graveyard_cinematic: 'graveyard_cinematic.png'
});


// Define the Characters
monogatari.characters({
	'olivia': {
		name: 'Olivia',
		color: '#e52b50',
		directory: 'olivia',
		sprites: {
			olivia_uniform_angry: 'olivia_uniform_angry.png',
			olivia_uniform_happy2: 'olivia_uniform_happy2.png',
			olivia_uniform_neutral: 'olivia_uniform_neutral.png',
			olivia_uniform_worried: 'olivia_uniform_worried.png',
			olivia_uniform_angry_closed_eyes: 'olivia_uniform_angry_closed_eyes.png',
			olivia_uniform_laugh: 'olivia_uniform_laugh.png',
			olivia_uniform_smile: 'olivia_uniform_smile.png',
			olivia_uniform_happy: 'olivia_uniform_happy.png',
			olivia_uniform_laugh2: 'olivia_uniform_laugh2.png',
			olivia_uniform_wink: 'olivia_uniform_wink.png',
			olivia_sport_angry: 'olivia_sport_angry.png',
			olivia_sport_angry_eyes_closed: 'olivia_sport_angry_eyes_closed.png',
			olivia_sport_happy: 'olivia_sport_happy.png',
			olivia_sport_happy2: 'olivia_sport_happy2.png',
			olivia_sport_laugh: 'olivia_sport_laugh.png',
			olivia_sport_neutral: 'olivia_sport_neutral.png',
			olivia_sport_wink: 'olivia_sport_wink.png',
			olivia_sport_worried: 'olivia_sport_worried.png',
			olivia_sport_worried2: 'olivia_sport_worried2.png',
			olivia_sport_worried3: 'olivia_sport_worried3.png',
			olivia_middle_flustered: 'olivia_middle_flustered.png',
			olivia_middle_flustered2: 'olivia_middle_flustered2.png',
			olivia_middle_happy: 'olivia_middle_happy.png',
			olivia_middle_sad2: 'olivia_middle_sad2.png',
			olivia_middle_sad3: 'olivia_middle_sad3.png',
			olivia_middle_shout: 'olivia_middle_shout.png',
			olivia_middle_shout2: 'olivia_middle_shout2.png',
			olivia_last_happy: 'olivia_last_happy.png',
			olivia_last_neutral: 'olivia_last_neutral.png',
			olivia_last_shout: 'olivia_last_shout.png',
			olivia_last_shout2: 'olivia_last_shout2.png',
			olivia_last_shout3: 'olivia_last_shout3.png',
			olivia_last_worried: 'olivia_last_worried.png',
			olivia_last_worried2: 'olivia_last_worried2.png',
			olivia_last_worried3: 'olivia_last_worried3.png',
			olivia_last_worried4: 'olivia_last_worried4.png',
			olivia_first_happy: 'olivia_first_happy.png',
			olivia_first_neutral: 'olivia_first_neutral.png',
			olivia_first_sad: 'olivia_first_sad.png',
			olivia_first_sad_crying: 'olivia_first_sad_crying.png',
			olivia_first_shouting: 'olivia_first_shouting.png',
			olivia_first_shouting2: 'olivia_first_shouting2.png',
			olivia_first_wink: 'olivia_first_wink.png',
			olivia_first_worried: 'olivia_first_worried.png',
			olivia_first_worried2: 'olivia_first_worried2.png',
		},
	},
	'speaker': {
		name: 'Trener',
		color: '#6167ff',
	},
	'students': {
		name: 'Uczniowie',
		color: '#9FBF99',
	},
	'studentA': {
		name: 'Uczennica A',
		color: '#9FBF99',
	},
	'studentB': {
		name: 'Uczennica B',
		color: '#9FBF99',
	},
	'studentC': {
		name: 'Uczeń C',
		color: '#9FBF99',
	},
	'oliviaMom': {
		name: 'Mama Olivii',
		color: '#9e3029',
	},
	'narrator': {
		name: '',
		color: '#739400',
	},
	'coach': {
		name: 'Trener',
		color: '#6167ff', // TODO set color
	},
	'aspen': {
		name: 'Aspen',
		color: '#8db600',
		directory: 'aspen',
		sprites: {
			aspen_casual_crying: 'aspen_casual_crying.png',
			aspen_casual_happy: 'aspen_casual_happy.png',
			aspen_casual_happy_sad: 'aspen_casual_happy_sad.png',
			aspen_casual_happy_sad2: 'aspen_casual_happy_sad2.png',
			aspen_casual_neutral: 'aspen_casual_neutral.png',
			aspen_casual_sad: 'aspen_casual_sad.png',
			aspen_casual_sad_mouth_open: 'aspen_casual_sad_mouth_open.png',
			aspen_sport_angry: 'aspen_sport_angry.png',
			aspen_sport_angry_shout: 'aspen_sport_angry_shout.png',
			aspen_sport_green: 'aspen_sport_green.png',
			aspen_sport_happy: 'aspen_sport_happy.png',
			aspen_sport_happy2: 'aspen_sport_happy2.png',
			aspen_sport_happy_sweaty: 'aspen_sport_happy_sweaty.png',
			aspen_sport_neutral: 'aspen_sport_neutral.png',
			aspen_sport_sad: 'aspen_sport_sad.png',
			aspen_sport_sad_shout: 'aspen_sport_sad_shout.png',
			aspen_sport_wink: 'aspen_sport_wink.png',
			aspen_uniform_angry: 'aspen_uniform_angry.png',
			aspen_uniform_sweaty: 'aspen_uniform_sweaty.png',
			aspen_uniform_happy: 'aspen_uniform_happy.png',
			aspen_uniform_happy2: 'aspen_uniform_happy2.png',
			aspen_uniform_happy3: 'aspen_uniform_happy3.png',
			aspen_uniform_happy4: 'aspen_uniform_happy4.png',
			aspen_uniform_neutral: 'aspen_uniform_neutral.png',
			aspen_uniform_wink: 'aspen_uniform_wink.png',
			aspen_uniform_sassy: 'aspen_uniform_sassy.png',
			aspen_sport_sassy: 'aspen_sport_sassy.png',
		}
	}
});

function playVoice (actor, index, mute) {
	if (mute) {
		return function () {
			return true;
		};
	}
	// return `play voice ${actor}${String(index).padStart(3, '0')}.mp3`;
	return `play voice ${actor}${String(index)}.mp3`;
}

function reversibleTwoWay (func) {
	return {
		'Function': {
			'Apply': function () {
				return func();
			},
			'Reverse': function () {
				return func();
			}
		}
	};
}

function studentA (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('student_a_', index), false, true);
			}
		});
		monogatari.run(`studentA ${text}`, false);
		return false;
	});
}

function studentB (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('student_b_', index), false, true);
			}
		});
		monogatari.run(`studentB ${text}`, false);
		return false;
	});
}

function studentC (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('student_c_', index), false, true);
			}
		});
		monogatari.run(`studentC ${text}`, false);
		return false;
	});
}

function oliviaMom (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('olivia_mom_', index), false, true);
			}
		});
		monogatari.run(`oliviaMom ${text}`, false);
		return false;
	});
}

function coach (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('coach_', index), false, true);
			}
		});
		monogatari.run(`coach ${text}`, false);
		return false;
	});
}

function olivia (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('olivia_', index), false, true);
			}
		});
		monogatari.run(`olivia ${text}`, false);
		return false;
	});
}

function aspen (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('aspen_', index), false, true);
			}
		});
		monogatari.run(`aspen ${text}`, false);
		return false;
	});
}

function aspenNarrator (index, text, mute) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('aspen_narrator_', index, mute), false);
			}
		});
		monogatari.run(`narrator ${text}`, false);
		return false;
	});
}

function students (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('students_', index), false);
			}
		});
		monogatari.run(`students ${text}`, false);
		return false;
	});
}

function speaker (index, text) {
	return reversibleTwoWay(function () {
		monogatari.run('stop voice', false).then(function () {
			if (index) {
				return monogatari.run(playVoice('speaker_', index), false);
			}
		});
		monogatari.run(`speaker ${text}`, false);
		return false;
	});
}

// placeholders
var aspenNarratorIndex = 0;


monogatari.script({
	// The game starts here.
	'Start': [
		'jump Prolog',
	],

	'Prolog': [
		'show scene black', // strych? biurko z dziennikiem?
		'play music beginning with loop fade 1',
		aspenNarrator(1, 'Każdy ma swój sposób na obchodzenie rocznic. Niezależnie czy jest nią rocznica ślubu, urodziny, czy Boże Narodzenie.'),
		aspenNarrator(2, 'Czekamy cały rok na coś, co ma nam podsumować ostatnie dwanaście miesięcy i dodać sił na przeżycie kolejnych.'),
		aspenNarrator(3, 'Może jest to kartka z życzeniami, której czułe słowa łagodzą ostatnie smutniejsze chwile. Może jest to czas spędzany razem z rodziną. Może chwila poświęcona refleksji.'),
		aspenNarrator(4, 'Moim sposobem jest utrwalanie wspomnień.'),
		aspenNarrator(5, 'Co rok siadam do lektury mojego starego pamiętnika.'),
		aspenNarrator(6, 'Dzisiaj po raz szesnasty.'),
		'stop music with fade 1',
		'wait 1000',
		'jump Chapter1',
	],

	'Chapter1': [
		'show scene busStop',
		'play music calm with loop fade 1',
		'play sound traffic with loop',
		'show character aspen aspen_uniform_sweaty at left',
		aspen(1, '...'), //dyszenie
		'show character olivia olivia_uniform_happy2 at right',
		olivia(1, 'Jak tak dalej pójdzie, to nie prześcigniesz nawet mojej babci!'),
		'show character aspen aspen_uniform_angry at left',
		aspen(2, 'Zobaczysz, zetrę ci ten uśmiech z twarzy!'),
		'show character olivia olivia_uniform_happy2 at right',
		olivia(2, 'Najpierw musisz mnie dogonić!'),
		'show character olivia olivia_uniform_laugh2 at right',
		olivia(3, 'Pierwsza!'),
		'show character aspen aspen_uniform_sweaty at left',
		aspen(3, 'Wszystko przez to, że dałem ci fory!'), //zadyszany
		'show character olivia olivia_uniform_laugh at right',
		'show character aspen aspen_uniform_sweaty at left',
		olivia(4, 'Chciałbyś!'),
		aspenNarrator(7, 'Pierwszy września.', true),
		// aspenNarrator(8, 'Początek roku szkolnego i początek corocznej męki.'),
		aspenNarrator(9, 'Spędziłem z Olivią wszystkie lata podstawówki - i jak miało się wkrótce okazać - również gimnazjum.'),
		aspenNarrator(10, 'Mieszkała w sąsiedztwie. Nasi rodzice się przyjaźnili, więc poniekąd byliśmy na siebie skazani.'),
		aspenNarrator(11, 'Pech chciał, że oboje zapałaliśmy miłością do biegania. Droga z domu Olivii na przystanek autobusowy stała się naszym torem wyścigowym, a my zawodnikami bijącymi' +
		' na nim życiowe rekordy.'),
		aspenNarrator(12, 'Jednak nigdy nie udało mi się przeskoczyć bariery dzielącej mnie i Olivię.'),
		'stop sound traffic with fade 1',
		'show scene schoolEntrance',
		aspenNarrator(13, 'Drugi września.', true),
		'show character olivia olivia_uniform_neutral at right',
		'show character aspen aspen_uniform_neutral at left',
		olivia(5, 'Wybrałeś już klub sportowy?'),
		aspen(4, 'Tak, wybrałem. Sztafeta.'), // TODO nagraj jeszcze raz pod ekspresję
		'show character olivia olivia_uniform_happy at right',
		olivia(6, 'Oh, czyli będziemy razem w drużynie! To znaczy… jeśli tylko nie wylądujesz na ławce rezerwowych!'),
		'show character aspen aspen_uniform_sassy at left',
		aspen(5, 'Żebym tylko ja cię tam nie wysłał!'), // TODO nagraj jeszcze raz pod ekspresję
		'show character olivia olivia_uniform_wink at right',
		olivia(7, 'Nie mogę się doczekać!'),

		aspenNarrator(14, 'Olivia zawsze lubiła zachodzić mi za skórę. Nie miała pewnie złych intencji…'),
		aspenNarrator(15, 'Jednak wtedy nie zawsze potrafiłem to dostrzec.'),
		'jump Chapter2',
	],

	'Chapter2': [
		'show scene track_cinematic',
		'wait 3000',
		'',
		'show scene track',
		'gallery unlock training',
		'show character aspen aspen_sport_neutral at left',
		'show character olivia olivia_sport_neutral at right',
		olivia(8, '...'), // dyszy
		'show character olivia olivia_sport_laugh at right',
		'show character aspen aspen_sport_green at left',
		coach(1, 'Brawo Olivia! Jak na razie najlepszy czas spośród pierwszoklasistów.'),
		'show character olivia olivia_sport_neutral at right',
		'show character aspen aspen_sport_neutral at left',
		coach(2, 'Następna grupa na linię startu!'),
		'play sound whistle',
		aspenNarrator(16, 'Wtedy moją jedyną motywacją było przewyższenie Olivii.'),
		'show character aspen aspen_sport_happy_sweaty at left',
		aspen(6, '...'), // dyszenie
		'show character olivia olivia_sport_neutral at right',
		'show character aspen aspen_sport_sad at left',
		coach(3, 'Całkiem, całkiem. Następna grupa na linię startu!'),
		aspenNarrator(17, 'Jednak nigdy nie udało mi się przeskoczyć dzielącej nas bariery.'),
		'show character aspen aspen_sport_angry at left',
		'show character olivia olivia_sport_wink at right',
		olivia(9, 'Hej, głowa do góry. Następnym razem będziesz pierwszy w swojej grupie!'),
		aspenNarrator(18, 'Nie potrafiłem też pogodzić się z tym jak patrzyła się na mnie ze współczuciem za każdym razem gdy przegrywałem.'),
		'show character aspen aspen_sport_angry_shout at left',
		'show character olivia olivia_sport_worried at right',
		aspen(7, 'Wiem, że będę pierwszy. Nie muszę tego słyszeć od ciebie.'),
		'hide character aspen',
		'show character olivia olivia_sport_worried3 at right',
		olivia(10, 'Aspen...'),
		// aspenNarrator(19, 'A chciała mi jedynie pomóc.'),
		'jump Chapter3',
	],

	'Chapter3': [
		'show scene classroom',
		aspenNarrator(20, 'Szesnasty września.', true),
		aspenNarrator(21, 'Olivii nie było w szkole.'),
		aspenNarrator(22, 'Drogę na przystanek musiałem przebiec sam, a na zajęciach wpatrywałem się w pustą ławkę przed sobą.'),
		'show character aspen aspen_uniform_neutral at center',
		studentA(1, 'Hej Aspen. Dlaczego nie ma dzisiaj Olivii?'),
		'show character aspen aspen_uniform_angry at center',
		aspen(8, 'A skąd mam wiedzieć? Nie jestem jej niańką.'),
		studentA(2, 'Myślałam, że się przyjaźnicie… Cóż, nieważne.'),
		studentA(3, 'Mógłbyś przekazać jej moje notatki po drodze do domu?'),
		'show character aspen aspen_uniform_neutral at center',
		aspen(9, 'Eh... Dobra, przekażę.'),
		'show scene houses',
		aspenNarrator(23, 'Siedemnasty września.', true),
		'show character olivia olivia_uniform_smile at right',
		'show character aspen aspen_uniform_happy at left',
		olivia(11, 'Jeśli wygram dzisiejszy wyścig będziesz musiał przyznać, że za mną tęskniłeś!'),
		'show character aspen aspen_uniform_happy3 at left',
		aspen(10, 'Chyba śnisz! Wcale za tobą nie tęskniłem!'), // TODO nagraj pod ilustracje
		'show character olivia olivia_uniform_laugh2 at right',
		olivia(12, 'Start!'),
		'show character aspen aspen_uniform_angry at left',
		aspen(11, 'Hej, znowu oszukujesz!'),
		aspenNarrator(24, 'Nie musiała się posuwać do takich zagrywek. I tak zawsze wygrywała.'),
		'show background busStop',
		'show character olivia olivia_uniform_happy2 at right',
		olivia(13, 'Pierwsza!'), // zadyszana
		'show character aspen aspen_uniform_sweaty at left',
		aspen(12, 'Oszukiwałaś!'), // TODO sprawdz intencje
		'show character olivia olivia_uniform_happy at right',
		olivia(14, 'Teraz musisz się przyznać.'),
		aspen(13, 'Niby do czego?'),
		'show character olivia olivia_uniform_laugh at right',
		olivia(15, 'Brakowało ci mnie przez ten jeden dzień!'),
		'show character aspen aspen_uniform_sassy at left', // TODO sprawdź intencję w nagraniu
		aspen(14, 'Chciałabyś. Właściwie to nawet nie zauważyłem, że cię nie ma.'),
		// aspen(15, 'Właściwie to nawet nie zauważyłem, że cię nie ma.'),
		'show character olivia olivia_uniform_wink at right',
		olivia(16, 'I tak wiem, że kłamiesz.'),
		aspenNarrator(25, 'Nie ważne jak bardzo nie chciałem się do tego przyznać - Olivia była dla mnie ważna.'),
		aspenNarrator(26, 'Była moją przyjaciółką od lat i prawdopodobnie znała mnie lepiej, niż ja sam siebie.'),
		aspenNarrator(27, 'Nie zrażały jej moje wredne docinki, za to mnie kłuło każde jej kolejne zwycięstwo.'),
		aspenNarrator(28, 'Od początku roku szkolnego wygrała ze mną dwanaście razy.'),
		'jump Chapter4',
	],

	'Chapter4': [
		'show scene track',
		aspenNarrator(29, 'Dwudziesty drugi września.', true),
		'show character aspen aspen_sport_neutral at left',
		'show character olivia olivia_sport_neutral at right',
		aspen(16, 'Spóźniłaś się dzisiaj.'),
		olivia(17, 'Tak, tak. Zaspałam, wybacz.'),
		'show character aspen aspen_sport_angry at left',
		aspen(17, 'Znowu musiałem biec sam na przystanek. Może zacznę sobie to liczyć jako walkowery?'),
		'show character olivia olivia_sport_happy at right',
		olivia(18, 'A licz sobie jeśli chcesz. Jutro i tak znowu cię prześcignę!'),
		coach(4, 'Drużyna na miejsca!'),
		'show character aspen aspen_sport_neutral at left',
		aspen(18, 'Teraz twoja kolej.'),
		'show character olivia olivia_sport_happy2 at right',
		olivia(19, 'Faktycznie! Życz mi szczęścia!'),
		'show character aspen aspen_sport_wink at left',
		aspen(19, 'Złam nogę.'),
		'show character olivia olivia_sport_wink at right',
		olivia(20, 'Milusi jak zawsze!'),
		'play sound whistle',
		'show character olivia olivia_sport_worried2 at right',
		olivia(21, '...'), //dyszy
		'show character olivia olivia_sport_worried3 at right',
		olivia(22, 'Co się-'), // odgłos upadania, shake
		'hide character olivia',
		'play sound thump',
		'show character aspen aspen_sport_sad_shout at left',
		aspen(20, 'Olivia!'),
		coach(5, 'Bez paniki! Aspen, biegnij po pielęgniarkę.'),
		aspen(21, 'Tak jest!'),

		'stop music with fade 1',
		aspenNarrator(30, 'Tamtego dnia Olivia zemdlała na bieżni.'),
		'show scene black with fadeIn duration 10s', // co tu dać?
		'play music sad with loop fade 1',
		aspenNarrator(31, 'Trener mówił nam by nie panikować, ale nie dokończyliśmy już treningu i kazano nam wrócić wcześniej do domu.'),
		aspenNarrator(32, 'Po Olivię podobno przyjechali rodzice.'),
		aspenNarrator(33, 'Przez następne dwa tygodnie nie było jej w szkole.'),
		aspenNarrator(34, 'Trasa spod jej domu na przystanek nie była już trasą wyścigu, a jedynie drogą, którą przebywałem w ramach treningu.'),
		aspenNarrator(35, 'Zdałem sobie sprawę jak bardzo brakuje mi tego jak odwracała się do mnie na zajęciach, tylko po to by upewnić się, że nie wpatruje się w to,'+
		' co działo się za oknem sali lekcyjnej.'),
		aspenNarrator(36, 'Aż w końcu - jak gdyby nigdy nic - wróciła.'),

		'show scene houses',
		'stop music with fade 1',
		'play sound traffic with loop',
		'show character aspen aspen_uniform_neutral at left',
		'show character olivia olivia_uniform_happy at right',
		olivia(23, 'Spóźniony!'),
		'play music calm with loop fade 1',
		'show character aspen aspen_uniform_angry at left',
		aspen(22, 'Od dwóch tygodni nie odpisujesz na moje wiadomości i ot tak chcesz ustalać mi harmonogram dnia?'),
		'show character olivia olivia_uniform_happy2 at right',
		olivia(24, 'Jesteś dwie minuty po czasie! A to oznacza, że musimy biec dzisiaj jeszcze szybciej, niż zwykle. Nie mamy czasu do stracenia - start!'),
		aspen(23, 'Hej!'),
		aspenNarrator(37, 'Znowu próbowała wziąć mnie z zaskoczenia i podstępem wygrać wyścig.'),
		aspenNarrator(38, 'Pomyśleć, że drugi raz z rzędu dałem się na to nabrać...'),
		'show background busStop',
		'show character aspen aspen_uniform_sweaty at left',
		aspen(24, '...'), // dyszenie
		'show character aspen aspen_uniform_happy at left',
		aspen(25, 'Wygrałem… WYGRAŁEM!'),
		'show character olivia olivia_uniform_smile at right',
		olivia(25, 'T… Tym razem dałam ci fory…'), //zadyszana
		aspenNarrator(39, 'Tamtego dnia po raz pierwszy wygrałem z Olivią. Jeden do dwunastu.'),
		'stop sound traffic with fade 1',
		'jump Chapter5',
	],

	'Chapter5': [
		// - kolejna nieobecność dziewczyny (tym razem na kilka dni)
		'show scene track',
		aspenNarrator(40, 'Ósmy października.', true),
		'show character aspen aspen_sport_neutral at center',
		coach(6, 'Aspen, rozgrzej się dzisiaj porządnie - będziesz biegał z pierwszym składem.'),
		'show character aspen aspen_sport_happy2 at center',
		aspen(26, 'Tak jest!'),
		aspenNarrator(41, 'Wtedy przeniesienie z ławki rezerwowych do pierwszego składu było kamieniem milowym na drodze do spełnienia marzeń.'),
		aspenNarrator(42, 'Byłem tak zaaferowany swoją szansą na awans w drużynie, że nie zauważyłem pewnej istotnej rzeczy - Olivia nie wróciła na treningi po powrocie do szkoły.'),
		aspenNarrator(43, 'Dopiero po jakimś czasie dotarło do mnie, że to nie moje umiejętności miały mi zapewnić obecność w wyjściowym składzie - tylko brak jednego z członków drużyny.'),

		'show scene schoolRooftop', // TODO może lepiej gdzie indziej, nie w szkole? Wykorzystamy przy okazji lepiej ilustracje

		'stop music with fade 1',

		'show character aspen aspen_sport_angry at left',
		'show character olivia olivia_first_neutral at right',
		aspen(27, 'Dlaczego nie przychodzisz na treningi?'),
		'play music quarell with loop fade 1',
		olivia(26, 'Nie mam czasu. Muszę nadrobić zaległości. Mam stertę notatek do przepisania...'),
		aspen(28, 'Przecież przynosiłem na bieżąco notatki z zajęć.'),
		'show character olivia olivia_first_sad at right',
		olivia(27, 'Jakoś nie miałam czasu do nich zajrzeć.'),
		aspen(29, 'To co robiłaś przez całe dwa tygodnie?'),
		olivia(28, 'Umm… Grałam w gry?'),
		aspen(30, 'Kłamiesz. Ty nawet nie lubisz gier.'),
		olivia(29, 'Powoli się do nich przekonuję…'),
		'show character olivia olivia_first_worried at right',
		'show character aspen aspen_sport_angry_shout at left',
		aspen(31, 'A dawanie mi forów na trasie do przystanku autobusowego?'),
		'show character olivia olivia_first_shouting at right',
		olivia(30, 'Ja...'), // skonfundowane jąkanie
		aspen(32, 'Nie chcę twojej litości!'),
		aspen(33, 'Myślisz, że sam nie dałbym rady dostać się do pierwszego składu?'),
		'show character olivia olivia_first_shouting2 at right',
		olivia(31, 'A-aspen…'),
		aspen(34, 'Myślisz, że jak zwolnisz dla mnie miejsce to będę ci za to dziękował?'),
		aspen(35, 'Nie tak to miało wyglądać… Myślałem, że jesteśmy rywalami, ale ty zawsze patrzyłaś na mnie z góry!'),
		'show character olivia olivia_first_sad_crying at right',
		olivia(32, 'Nieprawda! Ja...'),
		aspen(36, 'Mam tego dosyć, tego pocieszania mnie za każdym razem - udawania, że mnie wspierasz!'),
		olivia(33, 'To nie tak! Ja-'),
		aspen(37, 'Nie potrzebuję cię!'),
		'show scene black',
		'stop music with fade 5',
		aspenNarrator(44, 'W tamto popołudnie zostawiłem ją samą na dachu szkoły - oniemiałą i ze łzami w oczach.'),
		aspenNarrator(45, 'Niczego później nie żałowałem bardziej.'),
		aspenNarrator(46, 'Następnego dnia nie pojawiła się w szkole.'),
		'jump Chapter6',
	],

	'Chapter6': [
		'show scene track',
		'play music calm with loop fade 1',
		studentA(4, 'Rozmawiałaś może wczoraj z Olivią?'),
		studentB(1, 'Nie. Dlaczego?'),
		studentA(5, 'Nie było jej na zajęciach…'),
		studentB(2, 'Myślisz, że znowu zemdlała w szkole?'),
		studentA(6, 'Nie mam pojęcia… Zaczynam się o nią martwić. Nie wyglądała ostatnio najlepiej. Na treningach też jej nie było.'),
		studentC(1, 'Kto? Olivia?'),
		studentB(3, 'Tak. Wiesz może czy coś się z nią wczoraj nie działo?'),
		studentC(2, 'Hmm… Nie widziałem się z nią ostatnio, ale Thomas wspominał, że mijał się z nią wczoraj na schodach.'),
		studentC(3, 'Podobno płakała…'),
		studentB(4, 'Myślicie, że to przez Aspena?'),
		studentC(4, 'Co? Dlaczego?'),
		studentA(7, 'Taa… Codziennie sobie dogryzają. Nie zdziwiłabym się gdyby jedno z nich przekroczyło granicę.'),
		'show character aspen aspen_sport_angry at center',
		aspen(38, 'Nie macie przypadkiem ważniejszych rzeczy do roboty? Od dziesięciu minut trwa rozgrzewka,' +
			' a wy jeszcze nie ruszyliście się z miejsc.'),
		'play sound whistle',
		coach(7, 'Koniec pogaduszek! Zawody już za dwa tygodnie, a jak na razie nie widzę żadnych postępów!'),
		coach(8, 'Na linię startu!'),
		'show character aspen aspen_sport_happy_sweaty at center',
		aspen(39, '...'), //dyszy
		coach(9, 'Co się dzieje Aspen? Z takim nastawieniem nie mogę cię wpuścić do pierwszego składu.'),
		'show character aspen aspen_sport_sad_shout at center',
		aspen(40, '... Tak, wiem. Przepraszam.'),
		aspen(41, 'Następnym razem bardziej się postaram.'),
		coach(10, 'Skup się chłopcze. Sztafeta to sport zespołowy - sam nie wygrasz wyścigu.'),
		coach(11, 'Dogadajcie się zanim następnym razem wejdziecie na bieżnię.'),

		aspenNarrator(47, 'Nie ważne jak bardzo nie chciałem tego przyznać - trener miał rację. Nadszedł czas abym schował swoją dumę do kieszeni i pomyślał o innych.'),
		aspenNarrator(48, 'Dlatego postanowiłem od razu po treningu pójść do domu Olivii.'),
		aspenNarrator(49, 'W końcu ona również była członkiem drużyny i musiałem ją przeprosić zanim ponownie staniemy do wyścigu.'),
		// przed domem Olivii
		'show scene houses',
		'play sound birds with loop fade 1',
		'show character aspen aspen_casual_neutral at center',
		oliviaMom(1, 'Dzień dobry Aspen. Znów przyniosłeś Olivii notatki? Jak miło z twojej strony.'),
		aspen(42, 'Właściwie to przyszedłem aby z nią porozmawiać… A-ale notatki też przyniosłem!'),
		aspen(43, 'Czy Olivia jest w domu?'),
		oliviaMom(2, 'Nie Słońce... Olivia jest w szpitalu…'),
		'stop music with fade 1',
		'stop sound birds with fade 1',
		'show scene black with fadeIn duration 1s',
		'play sound heart',
		'wait 2000',
		'jump Chapter7',
	],

	'Chapter7': [
		'show scene hospital', // szpital
		'play music nostalgic with loop fade 1',
		'show character aspen aspen_casual_sad at left',
		'show character olivia olivia_middle_flustered2 at right',
		olivia(34, 'Aspen! Przyszedłeś!'),
		aspen(44, 'Olivia j-ja…'),
		'show character olivia olivia_middle_happy at right',
		olivia(35, 'Cieszę się, że jesteś. Umierałam tutaj z nudów!'),
		aspen(45, 'Czy wszystko ok? Wiedziałaś, że przyjdę?'),
		olivia(36, 'Mama mi powiedziała! I spokojnie - nic mi nie jest, a przynajmniej nic poważnego.'),
		'show character aspen aspen_casual_sad_mouth_open at left',
		aspen(46, '“Nic poważnego”?! Jesteś w szpitalu!'),
		'show character olivia olivia_middle_sad2 at right',
		olivia(37, 'Nie dramatyzuj. Nic się nie dzieje. Podobno mam gorsze wyniki czegoś-tam, ale czuję się dobrze.'),
		olivia(38, 'Na pewno niedługo znowu wrócę do szkoły.'),
		'show character aspen aspen_casual_sad at left',
		aspen(47, 'Posłuchaj. Ja… chciałem cię przeprosić. No wiesz… za to na dachu...'),
		'show character olivia olivia_middle_sad3 at right',
		olivia(39, 'Nie gniewam się.'),
		'show character aspen aspen_casual_happy_sad2 at left',
		aspen(48, 'Naprawdę?'),
		olivia(40, 'Oczywiście!'),
		olivia(41, 'Nie znamy się od wczoraj - wiem, że nie potrafisz radzić sobie z emocjami.'),
		'show character olivia olivia_middle_happy at right',
		'show character aspen aspen_casual_happy_sad at left',
		olivia(42, 'Ale przyznaję - mogłeś wybrać łagodniejszy sposób na przyznanie się, że mnie lubisz…'),
		aspen(49, 'Że co?!'),
		olivia(43, 'Chodź, oprowadzę cię.'), // hahaha.mp3

		aspenNarrator(50, 'Tamtego dnia zwiedziłem z nią cały szpital, zupełnie jakby oprowadzała mnie po swoim nowym domu.'+
		' Mimo że wydawała się być pełna optymizmu, to miałem dziwne wrażenie, że jednak coś jest nie tak.'),
		aspenNarrator(51, 'Jej włosy nie lśniły jak wtedy, gdy siedziałem za nią w ławce na zajęciach.'),
		aspenNarrator(52, 'Jej uśmiech nie był tak promienny, jak ten, którym witała mnie co rano.'),
		aspenNarrator(53, 'Jednak błysk w jej oczach kazał mi wierzyć, że wszystko będzie dobrze.'),

		// TODO hol szpitala, dziedziniec, czy to jest coś innego niż wcześniej
		'show character aspen aspen_casual_neutral at left',
		'show character olivia olivia_middle_flustered at right',
		aspen(50, 'Kiedy wrócisz na treningi? Zawody już za dwa tygodnie, a... trudno mi to przyznać, ale bez ciebie nie damy rady.'),
		olivia(44, 'Nie przesadzaj. Macie świetnych zawodników w drużynie.' +
			' Na pewno dacie radę! Poza tym za bardzo nienawidzisz przegrywać żeby zawalić taki wyścig.'), // hihi
		'show character aspen aspen_casual_sad at left',
		aspen(51, 'No dobra - JA nie dam sobie bez ciebie rady...'),
		'show character aspen aspen_casual_happy_sad at left',
		aspen(52, 'Zawsze byłaś obok i wspierałaś mnie na swój... dziwny, pokręcony sposób. ' +
			'I teraz, gdy cię nie ma, to zdałem sobie sprawę… To zdałem sobie sprawę, że mi tego brakuje, ok?'),
		'show character olivia olivia_middle_happy at right',
		olivia(45, 'Hihihi.'),
		'show character aspen aspen_casual_sad_mouth_open at left',
		aspen(53, 'Nie śmiej się ze mnie!'),
		olivia(46, 'Wybacz, ale to tak do ciebie niepodobne... w sensie: mówienie o swoich uczuciach.'),
		aspen(54, 'Teraz to już możesz mieć pewność, że nigdy więcej się to nie powtórzy!'),
		'show character olivia olivia_middle_happy at right',
		'show character aspen aspen_casual_neutral at left',
		olivia(47, 'Co powiesz na obietnicę?'),
		aspen(55, '…Jaką?'),
		'show character olivia olivia_middle_happy at right',
		olivia(48, 'Ja zrobię wszystko żeby pojawić się na wyścigu! Nawet jeśli tylko na trybunach, ' +
			'a ty pobiegniesz najlepiej jak tylko potrafisz. Zgoda?'),
		'show character aspen aspen_casual_sad at left',
		aspen(56, '...'),
		aspen(57, 'Zgoda.'),
		'stop music with fade 5',

		aspenNarrator(54, 'Olivia nie wróciła do szkoły, jednak to nie zachwiało mojej wiary w dotrzymanie przez nią danej mi obietnicy.'),
		aspenNarrator(55, 'Spędziłem te dwa tygodnie przygotowując się do zawodów.'),
		aspenNarrator(56, 'Biegałem, ćwiczyłem, nawet zaprzyjaźniłem się z resztą drużyny.'),
		aspenNarrator(57, 'Wszystko po to aby tym razem Olivia nie musiała mnie pocieszać.'),
		'jump Chapter8',
	],

	'Chapter8': [
		'show scene track',
		'play music calm with loop fade 1',
		aspenNarrator(58, 'Dwudziesty czwarty października.', true),
		'show character aspen aspen_sport_neutral at center',
		studentA(8, 'Co się tak rozglądasz Aspen? Szukasz kogoś?'),
		studentC(5, 'Pewnie jego dziewczyna jest gdzieś na trybunach.'),
		'show character aspen aspen_sport_angry at center',
		aspen(58, 'To nie jest moja dziewczyna!'),
		studentB(5, 'Dajcie mu spokój. To są jego pierwsze zawody. Na pewno się stresuje.'),
		aspen(59, 'Wcale się nie stresuję.'),
		studentB(6, 'Spokojnie - każdy z nas przez to przechodził. Ja, na przykład, zaliczyłam falstart.'),
		studentA(9, 'Dokładnie. Ja potknęłam się tuż przed metą na swoich pierwszych zawodach.'),
		studentC(6, 'Ja nie miałem takich problemów. Od razu pobiegłem po złoto.'),
		studentA(10, 'Tak? A kto zamknął się w łazience na pięć minut przez rozpoczęciem ceremonii otwarcia?'),
		studentC(7, 'To było dawno i nieprawda!'),
		'show character aspen aspen_sport_happy at center',
		studentA(11, 'Hihihi.'),
		studentB(7, 'W każdym razie nie spinaj się za bardzo. Najwyżej będziesz miał co opowiadać przyszłym rocznikom.'),
		studentB(8, 'Poza tym… Widziałam Olivię na trybunach.'),
		'show character aspen aspen_sport_neutral at center',
		aspen(60, 'Co? Gdzie?'),
		students(1, 'Hihihi.', true),
		speaker(1, 'Zawodnicy na stanowiska.'),

		aspenNarrator(59, 'Od sygnału startowego niewiele pamiętam. Nie zapisałem nawet które miejsce wtedy zajęliśmy.'),
		aspenNarrator(60, 'Ustawiając się na swojej pozycji dostrzegłem Olivię siedzącą w drugim rzędzie razem ze swoimi rodzicami, '+
		'jednak po przekroczeniu przeze mnie mety już ich tam nie było.'),
		'show character aspen aspen_sport_sad at center',
		aspenNarrator(61, 'Jak tylko dowiedziałem się, że ostatnio była widziana jak wynosili ją z hali - od razu pobiegłem w stronę szpitala.'),
		'show scene black with fadeIn duration 3s',
		'stop music with fade 3',
		aspenNarrator(62, 'Noc spędziłem na jednym z krzeseł w poczekalni.'),
		aspenNarrator(63, 'Dopiero rano pozwolono mi się z nią zobaczyć.'),
		'jump Chapter9',
	],

	'Chapter9': [
		'show scene hospital',
		'show character olivia olivia_last_neutral at right',
		'show character aspen aspen_casual_sad at left',
		'play music sad with loop fade 1',
		olivia(49, 'To był świetny wyścig.'),
		aspen(61, '...'),
		'show character olivia olivia_last_happy at right',
		olivia(50, 'Naprawdę przeszedłeś sam siebie.'),
		'show character olivia olivia_last_worried3 at right',
		'show character aspen aspen_casual_sad_mouth_open at left',
		aspen(62, 'Olivia... Dlaczego nie powiedziałaś mi prawdy?'),
		'show character olivia olivia_last_worried4 at right',
		olivia(51, '... Już wiesz?'),
		aspen(63, 'Tak.'),
		'show character olivia olivia_last_worried at right',
		aspen(64, 'Dlaczego nie powiedziałaś mi że...'),
		'show character olivia olivia_last_shout2 at right',
		'show character aspen aspen_casual_crying at left',
		olivia(52, 'Bo nie chciałam żebyś patrzył na mnie tak jak moi rodzice.'),
		olivia(53, 'Nie chciałam żebyś przez to traktował mnie inaczej...'),
		olivia(54, '...'),
		'show character olivia olivia_last_shout3 at right',
		olivia(55, 'Chciałam do końca ścigać się z tobą w drodze na przystanek...'), //rozkleja się
		'show scene black with fadeIn duration 3s',
		'jump Epilog'
	],

	'Epilog': [
		'gallery unlock hospital',
		'show scene hospital_cinematic',
		aspenNarrator(64, 'Dwudziesty siódmy listopada.', true),
		aspenNarrator(65, 'Odwiedzałem Olivię codziennie po zajęciach.'),
		aspenNarrator(66, 'Codziennie przebywałem trasę ze szkoły do szpitala truchtem, wierząc, że gdy znowu staniemy do wyścigu, na przystanku autobusowym, tym razem wygram uczciwie.'),
		aspenNarrator(67, 'Do tego czasu ścigaliśmy się na korytarzach, między pozostałymi pacjentami i pielęgniarkami. Karciły nas za to, ale wiem że w sekrecie nam kibicowały.'),
		aspenNarrator(68, 'Nie liczyłem ile razy wygrałem.'),
		aspenNarrator(69, 'Dla mnie liczył się tylko jeden wynik: jeden do dwunastu.'),
		'show scene black with fadeIn duration 1s',
		'wait 1000',
		aspenNarrator(70, 'Za każdym razem, gdy czytałem ten dziennik chciałem cofnąć się w czasie i naprawić wiele rzeczy,'+
		' odwołać każde przykre słowo, docenić to, czego nie potrafiłem wtedy dostrzec.'),
		'gallery unlock graveyard',
		'show scene graveyard_cinematic with fadeIn 1',
		'wait 3000',
		aspenNarrator(71, 'I choć nie pamiętam czy z tamtych zawodów wróciłem z medalem, to mam pewność, że możesz być ze mnie dumna, bo odczytałem ten dziennik po raz ostatni.'),
		aspenNarrator(72, 'Olivio, byłaś mi najbliższą przyjaciółką i najlepszą rywalką. Nadal będę nosił cię w sercu, jednak już czas bym ruszył naprzód.'),
		'stop music with fade 1',
		aspenNarrator(73, 'Kiedyś spotkamy się ponownie.'),
		'play music ending',
		'',
		'wait 5000',
		'..::Koniec::..',
		'end'
	],
});
