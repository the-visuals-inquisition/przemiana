'use strict';

// The name of your game, no spaces or special characters.
const name = 'Monogatari';

// The version of the cache, changing this will force everything to be cached
// again.
const version = '0.1.0';

const files = [

	'/',

	// General Files
	'manifest.json',

	// Engine Files
	'engine/core/monogatari.css',
	'engine/core/monogatari.js',

	// HTML Files
	'index.html',

	// Style Sheets
	'style/main.css',

	// JavaScript Files
	'js/options.js',
	'js/storage.js',
	'js/script.js',
	'js/main.js',

	// App Images
	'favicon.ico',
	'assets/icons/icon_48x48.png',
	'assets/icons/icon_60x60.png',
	'assets/icons/icon_70x70.png',
	'assets/icons/icon_76x76.png',
	'assets/icons/icon_96x96.png',
	'assets/icons/icon_120x120.png',
	'assets/icons/icon_128x128.png',
	'assets/icons/icon_150x150.png',
	'assets/icons/icon_152x152.png',
	'assets/icons/icon_167x167.png',
	'assets/icons/icon_180x180.png',
	'assets/icons/icon_192x192.png',
	'assets/icons/icon_310x150.png',
	'assets/icons/icon_310x310.png',
	'assets/icons/icon_512x512.png',

	// dubbing
	'assets/voices/aspen_1.mp3',
	'assets/voices/aspen_2.mp3',
	'assets/voices/aspen_3.mp3',
	'assets/voices/aspen_4.mp3',
	'assets/voices/aspen_5.mp3',
	'assets/voices/aspen_6.mp3',
	'assets/voices/aspen_7.mp3',
	'assets/voices/aspen_8.mp3',
	'assets/voices/aspen_9.mp3',
	'assets/voices/aspen_10.mp3',
	'assets/voices/aspen_11.mp3',
	'assets/voices/aspen_12.mp3',
	'assets/voices/aspen_13.mp3',
	'assets/voices/aspen_14.mp3',
	'assets/voices/aspen_15.mp3',
	'assets/voices/aspen_16.mp3',
	'assets/voices/aspen_17.mp3',
	'assets/voices/aspen_18.mp3',
	'assets/voices/aspen_19.mp3',
	'assets/voices/aspen_20.mp3',
	'assets/voices/aspen_21.mp3',
	'assets/voices/aspen_22.mp3',
	'assets/voices/aspen_23.mp3',
	'assets/voices/aspen_24.mp3',
	'assets/voices/aspen_25.mp3',
	'assets/voices/aspen_26.mp3',
	'assets/voices/aspen_27.mp3',
	'assets/voices/aspen_28.mp3',
	'assets/voices/aspen_29.mp3',
	'assets/voices/aspen_30.mp3',
	'assets/voices/aspen_31.mp3',
	'assets/voices/aspen_31_2.mp3',
	'assets/voices/aspen_32.mp3',
	'assets/voices/aspen_32_2.mp3',
	'assets/voices/aspen_33.mp3',
	'assets/voices/aspen_33_2.mp3',
	'assets/voices/aspen_34.mp3',
	'assets/voices/aspen_34_2.mp3',
	'assets/voices/aspen_35.mp3',
	'assets/voices/aspen_35_2.mp3',
	'assets/voices/aspen_36.mp3',
	'assets/voices/aspen_36_2.mp3',
	'assets/voices/aspen_37.mp3',
	'assets/voices/aspen_37_2.mp3',
	'assets/voices/aspen_38.mp3',
	'assets/voices/aspen_39.mp3',
	'assets/voices/aspen_40.mp3',
	'assets/voices/aspen_41.mp3',
	'assets/voices/aspen_42.mp3',
	'assets/voices/aspen_43.mp3',
	'assets/voices/aspen_44.mp3',
	'assets/voices/aspen_45.mp3',
	'assets/voices/aspen_46.mp3',
	'assets/voices/aspen_47.mp3',
	'assets/voices/aspen_48.mp3',
	'assets/voices/aspen_49.mp3',
	'assets/voices/aspen_50.mp3',
	'assets/voices/aspen_51.mp3',
	'assets/voices/aspen_52.mp3',
	'assets/voices/aspen_53.mp3',
	'assets/voices/aspen_54.mp3',
	'assets/voices/aspen_55.mp3',
	'assets/voices/aspen_56.mp3',
	'assets/voices/aspen_57.mp3',
	'assets/voices/aspen_58.mp3',
	'assets/voices/aspen_59.mp3',
	'assets/voices/aspen_60.mp3',
	'assets/voices/aspen_61.mp3',
	'assets/voices/aspen_62.mp3',
	'assets/voices/aspen_63.mp3',
	'assets/voices/aspen_64.mp3',
	'assets/voices/coach_1.mp3',
	'assets/voices/coach_2.mp3',
	'assets/voices/coach_3.mp3',
	'assets/voices/coach_4.mp3',
	'assets/voices/coach_5.mp3',
	'assets/voices/coach_6.mp3',
	'assets/voices/coach_7.mp3',
	'assets/voices/coach_8.mp3',
	'assets/voices/coach_9.mp3',
	'assets/voices/coach_10.mp3',
	'assets/voices/coach_11.mp3',
	'assets/voices/olivia_1.mp3',
	'assets/voices/olivia_2.mp3',
	'assets/voices/olivia_3.mp3',
	'assets/voices/olivia_4.mp3',
	'assets/voices/olivia_5.mp3',
	'assets/voices/olivia_6.mp3',
	'assets/voices/olivia_7.mp3',
	'assets/voices/olivia_8.mp3',
	'assets/voices/olivia_9.mp3',
	'assets/voices/olivia_10.mp3',
	'assets/voices/olivia_11.mp3',
	'assets/voices/olivia_12.mp3',
	'assets/voices/olivia_13.mp3',
	'assets/voices/olivia_14.mp3',
	'assets/voices/olivia_15.mp3',
	'assets/voices/olivia_16.mp3',
	'assets/voices/olivia_17.mp3',
	'assets/voices/olivia_18.mp3',
	'assets/voices/olivia_19.mp3',
	'assets/voices/olivia_20.mp3',
	'assets/voices/olivia_21.mp3',
	'assets/voices/olivia_22.mp3',
	'assets/voices/olivia_23.mp3',
	'assets/voices/olivia_24.mp3',
	'assets/voices/olivia_25.mp3',
	'assets/voices/olivia_26.mp3',
	'assets/voices/olivia_27.mp3',
	'assets/voices/olivia_28.mp3',
	'assets/voices/olivia_29.mp3',
	'assets/voices/olivia_30.mp3',
	'assets/voices/olivia_31.mp3',
	'assets/voices/olivia_32.mp3',
	'assets/voices/olivia_33.mp3',
	'assets/voices/olivia_34.mp3',
	'assets/voices/olivia_35.mp3',
	'assets/voices/olivia_36.mp3',
	'assets/voices/olivia_37.mp3',
	'assets/voices/olivia_38.mp3',
	'assets/voices/olivia_39.mp3',
	'assets/voices/olivia_40.mp3',
	'assets/voices/olivia_41.mp3',
	'assets/voices/olivia_42.mp3',
	'assets/voices/olivia_43.mp3',
	'assets/voices/olivia_44.mp3',
	'assets/voices/olivia_45.mp3',
	'assets/voices/olivia_46.mp3',
	'assets/voices/olivia_47.mp3',
	'assets/voices/olivia_48.mp3',
	'assets/voices/olivia_49.mp3',
	'assets/voices/olivia_50.mp3',
	'assets/voices/olivia_51.mp3',
	'assets/voices/olivia_52.mp3',
	'assets/voices/olivia_53.mp3',
	'assets/voices/olivia_54.mp3',
	'assets/voices/olivia_55.mp3',
	'assets/voices/olivia_mom_1.mp3',
	'assets/voices/olivia_mom_2.mp3',
	'assets/voices/speaker_1.mp3',
	'assets/voices/student_c_1.mp3',
	'assets/voices/student_c_2.mp3',
	'assets/voices/student_c_3.mp3',
	'assets/voices/student_c_4.mp3',
	'assets/voices/student_c_5.mp3',
	'assets/voices/student_c_6.mp3',
	'assets/voices/student_c_7.mp3',
	'assets/voices/student_c_8.mp3',

	// characters
	'assets/characters/olivia/olivia_uniform_angry.png',
	'assets/characters/olivia/olivia_uniform_angry_closed_eyes.png',
	'assets/characters/olivia/olivia_uniform_happy.png',
	'assets/characters/olivia/olivia_uniform_happy2.png',
	'assets/characters/olivia/olivia_uniform_laugh.png',
	'assets/characters/olivia/olivia_uniform_laugh2.png',
	'assets/characters/olivia/olivia_uniform_neutral.png',
	'assets/characters/olivia/olivia_uniform_smile.png',
	'assets/characters/olivia/olivia_uniform_wink.png',
	'assets/characters/olivia/olivia_uniform_worried.png',
	'assets/characters/olivia/olivia_sport_angry_eyes_closed.png',
	'assets/characters/olivia/olivia_sport_happy.png',
	'assets/characters/olivia/olivia_sport_happy2.png',
	'assets/characters/olivia/olivia_sport_laugh.png',
	'assets/characters/olivia/olivia_sport_neutral.png',
	'assets/characters/olivia/olivia_sport_wink.png',
	'assets/characters/olivia/olivia_sport_angry.png',
	'assets/characters/olivia/olivia_middle_shout2.png',
	'assets/characters/olivia/olivia_middle_flustered.png',
	'assets/characters/olivia/olivia_middle_flustered2.png',
	'assets/characters/olivia/olivia_middle_happy.png',
	'assets/characters/olivia/olivia_middle_sad2.png',
	'assets/characters/olivia/olivia_middle_sad3.png',
	'assets/characters/olivia/olivia_middle_shout.png',
	'assets/characters/olivia/olivia_last_shout3.png',
	'assets/characters/olivia/olivia_last_worried.png',
	'assets/characters/olivia/olivia_last_worried2.png',
	'assets/characters/olivia/olivia_last_happy.png',
	'assets/characters/olivia/olivia_last_neutral.png',
	'assets/characters/olivia/olivia_last_shout.png',
	'assets/characters/olivia/olivia_last_shout2.png',
	'assets/characters/olivia/olivia_first_shouting2.png',
	'assets/characters/olivia/olivia_first_wink.png',
	'assets/characters/olivia/olivia_first_worried.png',
	'assets/characters/olivia/olivia_first_worried2.png',
	'assets/characters/olivia/olivia_first_happy.png',
	'assets/characters/olivia/olivia_first_neutral.png',
	'assets/characters/olivia/olivia_first_sad.png',
	'assets/characters/olivia/olivia_first_sad_crying.png',
	'assets/characters/olivia/olivia_first_shouting.png',
	'assets/characters/olivia/olivia_last_worried3.png',
	'assets/characters/olivia/olivia_last_worried4.png',
	'assets/characters/olivia/olivia_sport_worried.png',
	'assets/characters/olivia/olivia_sport_worried2.png',
	'assets/characters/olivia/olivia_sport_worried3.png',
	'assets/characters/aspen/aspen_uniform_angry.png',
	'assets/characters/aspen/aspen_uniform_sweaty.png',
	'assets/characters/aspen/aspen_uniform_happy.png',
	'assets/characters/aspen/aspen_uniform_happy2.png',
	'assets/characters/aspen/aspen_uniform_happy3.png',
	'assets/characters/aspen/aspen_uniform_happy4.png',
	'assets/characters/aspen/aspen_uniform_neutral.png',
	'assets/characters/aspen/aspen_uniform_wink.png',
	'assets/characters/aspen/aspen_sport_green.png',
	'assets/characters/aspen/aspen_sport_happy.png',
	'assets/characters/aspen/aspen_sport_happy_sweaty.png',
	'assets/characters/aspen/aspen_sport_happy2.png',
	'assets/characters/aspen/aspen_sport_neutral.png',
	'assets/characters/aspen/aspen_sport_sad.png',
	'assets/characters/aspen/aspen_sport_sad_shout.png',
	'assets/characters/aspen/aspen_sport_wink.png',
	'assets/characters/aspen/aspen_sport_angry.png',
	'assets/characters/aspen/aspen_sport_angry_shout.png',
	'assets/characters/aspen/aspen_casual_sad_mouth_open.png',
	'assets/characters/aspen/aspen_casual_crying.png',
	'assets/characters/aspen/aspen_casual_happy.png',
	'assets/characters/aspen/aspen_casual_happy_sad.png',
	'assets/characters/aspen/aspen_casual_happy_sad2.png',
	'assets/characters/aspen/aspen_casual_neutral.png',
	'assets/characters/aspen/aspen_casual_sad.png',

	// scenes
	'assets/scenes/hospital.png',
	'assets/scenes/track.png',
	'assets/scenes/track_cinematic.png',
	'assets/scenes/hospital_cinematic.png',
	'assets/scenes/graveyard_cinematic.png',
];

self.addEventListener ('install', (event) => {
	self.skipWaiting ();
	event.waitUntil (
		caches.open (`${name}-v${version}`).then ((cache) => {
			return cache.addAll (files);
		})
	);
});

self.addEventListener ('activate', (event) => {
	event.waitUntil (
		caches.keys ().then ((keyList) => {
			return Promise.all (keyList.map ((key) => {
				if (key !== `${name}-v${version}`) {
					return caches.delete (key);
				}
			}));
		})
	);
	return self.clients.claim ();
});

self.addEventListener ('fetch', (event) => {

	if (event.request.method !== 'GET') {
		return;
	}

	event.respondWith (
		caches.match (event.request).then ((cached) => {
			function fetchedFromNetwork (response) {
				const cacheCopy = response.clone ();

				caches.open (`${name}-v${version}`).then (function add (cache) {
					cache.put (event.request, cacheCopy);
				});
				return response;
			}

			function unableToResolve () {
				return new Response (`
					<!DOCTYPE html><html lang=en><title>Bad Request</title><meta charset=UTF-8><meta content="width=device-width,initial-scale=1"name=viewport><style>body,html{width:100%;height:100%}body{text-align:center;color:#545454;margin:0;display:flex;justify-content:center;align-items:center;font-family:-apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen,Ubuntu,Cantarell,"Open Sans","Fira Sans","Droid Sans","Helvetica Neue",sans-serif}h1,h2{font-weight:lighter}h1{font-size:4em}h2{font-size:2em}</style><div><h1>Service Unavailable</h1><h2>Sorry, the server is currently unavailable or under maintenance, try again later.</h2></div>
				`, {
					status: 503,
					statusText: 'Service Unavailable',
					headers: new Headers ({
						'Content-Type': 'text/html'
					})
				});
			}

			const networked = fetch (event.request)
				.then (fetchedFromNetwork, unableToResolve)
				.catch (unableToResolve);

			return cached || networked;
		})
	);
});
